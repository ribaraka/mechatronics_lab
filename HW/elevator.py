"""@file        elevator.py
@brief          Simulates a two-story elevator
@details        Implements a finite state machine, shown below, to simulate the
                behavior of an elevator.
                
                Source Code:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/HW/elevator.py
                
                Finite State Machine:
                @image html elevator_fsm.jpg
                
@author         Richard Barakat
@date           1/27/21
"""

import time
import random

def motor_cmd(cmd):
    '''
    @brief     This function controls motor output at a specific input.
    @param cmd An integer specifying the desired operating condition of the 
               motor
    '''

    if cmd==0:
        print('Mot stop')
    elif cmd==1:
        print('Mot up')
    elif cmd==2:
        print('Mot down')

def floor_1():
    '''
    @brief     This function detects elevator presence on first floor
    '''
    return random.choice([True, False]) # randomly returns T or F

def floor_2():
    '''
    @brief     This function detects elevator presence on second floor
    '''
    return random.choice([True, False]) # randomly returns T or F

def button_1():
    '''
    @brief     This function randomly detects button 1 presses by the user
    '''
    return random.choice([True, False]) # randomly returns T or F

def button_2():
    '''
    @brief     This function randomly detects button 2 presses by the user
    '''
    return random.choice([True, False]) # randomly returns T or F

# main program / test program begin
#   this code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a module
if __name__ == '__main__':
    # program intialization goes here
    state = 0 # Initial state is the init state
    
    while True:
        try:
            # main program code goes here
            if state==0:
                # run state 0 (moving down) code
                print('S0 - Moving Down')
                # if we are on the 1st floor, stop motor and reset button 1
                # then transition to S2
                if floor_1():
                    motor_cmd(0)  # command motor to stop
                    button_1()==0 # command button 1 to reset
                    state=1 # updating state for next iteration
            
            elif state==1:
                # run state 1 (stopped on Floor 1) code
                print('S1 - Stopped on Floor 1')
                # if button 2 pressed, run the motor up and transition to S2
                if button_2():
                    motor_cmd(1) # command motor to go up
                    state=2
                # if button 1 pressed, reset button 1 and stay at S1
                elif button_1(): 
                    button_1()==0 # reset button 1
                                
            elif state==2:
                # run state 2 (moving up) code
                print('S2 - Moving Up')
                # if we are on 2nd floor, stop the motor and reset button 3
                # then transition to S3
                if floor_2():
                    motor_cmd(0) # command motor to stop
                    button_2()==0 # reset button 2
                    state=3
            
            elif state==3:
                # run state 3 (stopped on Floor 2) code
                print('S3 - Stopped on Floor 2')
                # if button 1 pressed, run the motor down and transition to S2
                if button_1():
                    motor_cmd(2) # command motor to go down
                    state=0
                # if button 2 pressed, reset button 1 and stay at S1
                elif button_2():
                    button_2()==0 # reset button 2
            
            # slow down execution of FSM so we can see output in console
            time.sleep(0.2)
            
        except KeyboardInterrupt:
                # this except block catches "Ctrl-C" from keyboard to break
                break
