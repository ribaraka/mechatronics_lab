"""@file        Fibonacci.py
@brief          Calculates Fibnoacci number for corresponding index
@details        Implements bottom-up approach to calculate Fibonacci number at
                each index inputted by user.
                
                Source Code:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab%201/Fibonacci.py
@author         Richard Barakat
@date           1/20/21
"""

def fib (idx):
    '''
    @brief     This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
               Fibonacci number
    '''

    # inital seed values
    fn_2 = 0 # Fibonacci number at n=0
    fn_1 = 1 # Fibonacci number at n=1
    
    # Fibonacci number at n=0 is 0
    if idx == 0:
        return fn_2
    # Fibonacci number at n=1 is 1    
    elif idx == 1:
        return fn_1
    # Evaluate Fibonacci number for n>1   
    else:
        # initalize counter
        i = 1   
        # loop through each integer
        while i < idx: # 1st iteration computes Fibonacci number, fn, at n=2
            # increase counter +1
            i += 1
            # calculate Fibonacci number
            fn = fn_2 + fn_1 # fn_2 = Fib at n-2, fn_1 = Fib at n-1
            # shift indeces of previous Fibonacci numbers +1 integer
            fn_2 = fn_1
            fn_1 = fn
        return fn

# user interface

# first while loop asks user for integer and rejects negative integers
first = True
while first:
    try:
        # receive user input
        user = int(input('Please enter an index of interest: '))
        
        # reject negative integers
        if user < 0:
            print('ERROR: please input an integer greater than or equal to 0')    
        
        # evaluate fib(idx) for integers >= 0
        else:
            print('Fibonacci number at index',user,'is',fib(user)) 
    
            # second while loop asks user if they want to select another integer or exit program
            # loop only starts for inputs >= 0
            second = True
            while second:
                # receive user input
                followup = input('Would you like to enter another integer? ').lower()
                
                # continue first while loop if "yes"
                if followup == 'yes':
                    first = True
                    second = False
                
                # break first and second while loops if "no"
                elif followup == 'no':
                    first = False
                    second = False
                
                # continue second while loop until "yes" or "no" input received
                else:
                    print('ERROR: please type "yes" to enter another integer or "no" to leave.')
                    first = False
                    second = True
    
    # reject non-integer inptuts (inputs that result in error in line 49)
    except ValueError:
        print('ERROR: please input an integer greater than or equal to 0') 