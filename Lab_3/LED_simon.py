"""@file        LED_simon.py
@brief          Compares randomly generated pattern to user input pattern
@details        Implements two finite state machines, shown below, to display
                a random LED pulse sequence and detect if the user can 
                replicate the same sequence.
                
                Video:
                https://drive.google.com/file/d/1incWY9ugrqNVYEgmzWRRsACqXyZXpg7s/view?usp=sharing
                
                Class Reference Page:
                https://ribaraka.bitbucket.io/classLEDtask_1_1LEDtask.html
                
                Main Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_3/LED_simon.py
                
                Class Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_3/LEDtask.py
                
                Finite State Machine - Rounds:
                @image html rounds_fsm.jpg
                
                Finite State Machine - Blink Pattern:
                @image html blink_fsm.jpg
@author         Richard Barakat
@date           2/18/21
"""

# import LED class file
from LEDTask import LEDtask

# Program initialization
task = LEDtask()

# Print welcome message and how-to instructions (print is split bc autowrap in PuTTY)
print('Welcome to the Simon Says Blinking LED Game! A randomly generated LED pulse')
print('pattern will be generated, and you must replicate the pattern over three rounds.')      
print('After each round, the sequence will become longer. After the random sequence is')
print('displayed, press the blue button to start your entry. Good Luck!')

while True:
    try:
        # execute code
        task.roundFSM()
        
        # exit program if user chooses to quit game after round
        if task.exit == 1:
            print('Thanks for Playing!')
            break
        
    except KeyboardInterrupt:
        # This except block catches "Ctrl-C" from the keyboard to end the
        # while(True) loop when desired
        print('Ctrl-c has been pressed')
        break
