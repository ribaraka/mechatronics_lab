import random
import utime
import pyb

## @brief Defines output pin associated with LED
#  @details This variable defines pin A5 on the Nucleo which is used for 
#           performing the PWM
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## @brief Defines timer to be used for LED
#  @details This variable defines Timer 2 to be used at 10000Hz for the LED
tim2 = pyb.Timer(2, freq = 10000)
## @brief Defines channel to be used for LED
#  @details This variable defines channel 1 to be used to perform PWM with pin
#           A5
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## @brief Defines input pin associated blue button, B1
#  @details This variable defines pin C13 on the Nucleo which corresponds to 
#           the blue user button
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

class LEDtask:
    ''' @brief          LEDtask is the LED simon says game.
        @details        An object of class LEDtask.LEDtask can generate a
                        random LED pattern and detect that type of LED blink
                        inputted by the user. LEDtask can compare the user 
                        input to the randomly generated pattern and determine 
                        if the user pattern matches the random pattern. The
                        LEDtask objects can be used simultaneously to run 
                        multiple games at once.
                        
                        Class Source:
                        https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_3/LEDtask.py
    '''
    
    # Static variables are defined outside of methods
    
    ## State constants
    S0_INIT         = 0
    S1_NO_PRESS     = 1
    S2_SHORT_PRESS  = 2
    S3_LONG_PRESS   = 3
    R0_INIT         = 0
    R1_ROUND_1      = 1
    R2_ROUND_2      = 2
    R3_ROUND_3      = 3
    
    def __init__(self):
        # This not your state 0
        
        ## The current state of the finite state machine
        self.state = 0
        
        ## The current round of the finite state machine
        self.round = 0
               
        # The trigger that controls whether the game loop starts
        self.start = 1
        
        # limits string print to only once
        self.go = 1
        
        # limits recorded start time to one instant
        self.roundstart = 1
        
        ## The current start time for each type of LED pulse
        #  could eliminate for concision if desired
        self.startTime = 0
        
        ## The current start time for the user inputs
        self.UserStartTime = 0
        
        ## The current press time
        self.pressTime = 0
        
        ## The current release time
        self.releaseTime = 0
        
        ## The duration over which the user is pressing the button
        self.userpresstime = 0
        
        ## The duration between button presses
        self.userbetweentime = 0
        
        ## The current state of the button press indicator
        self.buttonpress = 0
        
        ## The current state of the pulse detector trigger
        self.startdetector = 0        
        
        ## The current state of the pulse pattern trigger
        self.next = 0
        
        ## The current state of the random pulse counter
        self.patterncounter = 0
        
        ## The current state of the user input pulse counter
        self.userpatterncounter = 0
        
        ## Stores user inputs
        self.userpattern = []
        
        ## The current state of the input pulse detector
        self.detectLED = 0
        
        # Indictes when it's ready to append user inputs 
        self.appendready = 0
        
        ## The current state of win counter
        self.win = 0
        
        ## The current state of lose counter
        self.lose = 0
        
        ## The current state of the eit program indicator
        self.exit = 0
        
        self.ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.ButtonPress)
        
    def roundFSM(self):
        '''
        @brief     This is the FSM for the three rounds of the game.
        '''           
        if self.round==self.R0_INIT:
            if self.start == 1:
                # trun off if statement
                self.start = 0
                ## The random LED pattern
                self.PatternGenerator()
                print('Press blue button to start Round 1')
            # if button is pressed, transition to state 1
            if self.buttonpress >= 1:
                print('Round 1!')
                self.buttonpress = 0 # reset button
                self.startdetector = 0 # reset detector trigger
                # send to Round 1
                self.RoundTransitionTo(self.R1_ROUND_1)

        if self.round==self.R1_ROUND_1:
            # display pattern and receive user pattern
            self.DisplayAndDetect(5)
            # if user pattern doesn't match random pattern
            if self.randompattern[0:self.userpatterncounter] != self.userpattern:
                # count loss
                self.LOSE()
                # run reset variables function
                self.reset()
                print('You Lose Round 1 :(')
                # ask user if they wnat to try again or exit
                self.FollowupAfterLoss()
            # if user reaches 5 inputs without mistake
            if self.userpatterncounter == 5:
                # compare random pattern to user pattern
                if self.randompattern[0:5] == self.userpattern:
                    # count win
                    self.WIN()
                    # run reset variables function
                    self.reset()
                    print('You Win Round 1!')
                    # ask user if they wnat to continue to next round or exit
                    self.FollowupAfterRoundWin(self.R2_ROUND_2)
                    
        if self.round==self.R2_ROUND_2:
            # display pattern and receive user pattern
            self.DisplayAndDetect(10)
            # if user pattern doesn't match random pattern
            if self.randompattern[0:self.userpatterncounter] != self.userpattern:
                # count loss
                self.LOSE()
                # run reset variables function
                self.reset()
                print('You Lose Round 2 :(')
                # ask user if they wnat to try again or exit
                self.FollowupAfterLoss()
            # if user reaches 10 inputs without mistake
            if self.userpatterncounter == 10:
                # compare random pattern to user pattern
                if self.randompattern[0:10] == self.userpattern:
                    # count win
                    self.WIN()
                    # run reset variables function
                    self.reset()
                    print('You Win Round 2!')
                    # ask user if they wnat to continue to next round or exit
                    self.FollowupAfterRoundWin(self.R3_ROUND_3)
                    
        if self.round==self.R3_ROUND_3:
            # display pattern and receive user pattern
            self.DisplayAndDetect(15)
            # if user pattern doesn't match random pattern
            if self.randompattern[0:self.userpatterncounter] != self.userpattern:
                # count loss
                self.LOSE()
                # run reset variables function
                self.reset()
                print('You Lose Round 3 :(')
                # ask user if they wnat to try again or exit
                self.FollowupAfterLoss()
            # if user reaches 15 inputs without mistake
            if self.userpatterncounter == 15:
                # compare random pattern to user pattern
                if self.randompattern[0:15] == self.userpattern:
                    # count win
                    self.WIN()
                    # run reset variables function
                    self.reset()
                    print('CONGRATULATIONS! YOU WIN!!!')
                    # ask user if they wnat to continue to play again or exit
                    self.FollowupAfterGameWin()
                    
    def patternFSM(self):
        '''
        @brief     This is the FSM for the randomly generated pulse pattern.
        '''
        if self.state==self.S0_INIT:
            # run pulse transition function
            self.LEDtransition()
            
        elif self.state==self.S1_NO_PRESS:
            # run state 1 (no press) code
            self.led(0)
            # run pulse transition function
            self.LEDtransition()
            
        elif self.state==self.S2_SHORT_PRESS:
            # run state 2 (short press) code
            self.led(1)
            # run pulse transition function
            self.LEDtransition()
            
        elif self.state==self.S3_LONG_PRESS:
            # run state 3 (long press) code
            self.led(2)
            # run pulse transition function
            self.LEDtransition()
            
        else:
            pass
            # code to run if state number is invalid
            # program should ideally never reach here
    
    def StateTransitionTo(self, newState):
        '''
        @brief     This function controls transitions between blink FSM states.
        @param cmd The new state that the FSM is transitioning to.
        '''
        self.state = newState
        
    def RoundTransitionTo(self, newRound):
        '''
        @brief     This function controls transitions between Round FSM states.
        @param cmd The new round that the FSM is transitioning to.
        '''
        self.round = newRound
      
    def led(self,cmd):
        '''
        @brief     This function controls LED blink at a specific input.
        @param cmd A string specifying the desired operating condition of the 
                   LED
        '''
        # no press (1 second)
        if cmd == 0:
            # turn led off
            if utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 < 1:
                t2ch1.pulse_width_percent(0) 
            elif utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 > 1:
                # trigger pattern counter
                self.PatternCounter()
                # turn on LEDtransition if statement
                self.next = 1
            
        # short press (1 second)
        elif cmd == 1:
            if utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 < 1:
                t2ch1.pulse_width_percent(100) 
            # turn LED off for 0.2 seconds
            elif utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 > 1 and utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 < 1+0.2:
                t2ch1.pulse_width_percent(0) 
            elif utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 > 1+0.2:
                # trigger pattern counter
                self.PatternCounter()
                # turn on LEDtransition if statement
                self.next = 1
            
        # long press (3 seconds)
        elif cmd == 2:
            if utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 < 3:
                t2ch1.pulse_width_percent(100) 
            # turn LED off for 0.2 seconds
            elif utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 > 3 and utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 < 3+0.2:
                t2ch1.pulse_width_percent(0) 
            elif utime.ticks_diff(utime.ticks_ms(),self.startTime)/1000 > 3+0.2:
                # trigger pattern counter
                self.PatternCounter()
                # turn on LEDtransition if statement
                self.next = 1
       
    def LEDtransition(self):
        '''
        @brief     This function transitions from one pulse to the next.
        '''
        if self.next == 1:
            # turn off if statement (truns back on after pulse in def led)
            self.next = 0
            # read pulse of random pattern and trigger appropriate pulse
            if self.randompattern[self.patterncounter] == 0:
                self.startTime = utime.ticks_ms()
                self.StateTransitionTo(self.S1_NO_PRESS)
            elif self.randompattern[self.patterncounter] == 1:
                self.startTime = utime.ticks_ms()
                self.StateTransitionTo(self.S2_SHORT_PRESS)
            elif self.randompattern[self.patterncounter] == 2:
                self.startTime = utime.ticks_ms()
                self.StateTransitionTo(self.S3_LONG_PRESS)
        
    def DisplayAndDetect(self, numberofpulses):
        '''
        @brief This function displays random pattern and stores user pattern.
        @param The number of pulses per round
        '''
        if self.roundstart == 1:
            # turn if statement off
            self.roundstart = 0
            # turn on patternFSM
            self.next = 1
        # run randomly generated pattern
        self.patternFSM()
        # after x number of pulses
        if self.patterncounter == numberofpulses:     
            # send patternFSM back to state 0
            self.StateTransitionTo(self.S0_INIT)
            if self.go == 1:
                # turn if statement off
                self.go = 0
                # reset button to rid of an user presses during pattern display
                self.buttonpress = 0
                # create empty matrix that stores user inputs
                self.userpattern = []    
                ## allowable error for user inputs
                self.tolerance = 0.5
                print('Press blue button to start your entry')
            # if button pressed
            if self.buttonpress == 2:
                # reset button to prepare for user inputs
                self.buttonpress = 0
                # record the button release time when the user pattern will start
                self.releaseTime = utime.ticks_ms()
                # turn pulse detection trigger on
                self.startdetector = 1
            # run function to detect user inputs
            self.PulseDetector()
    
    def PulseDetector(self):
        '''
        @brief     This function detects the pulse type of user inputs
        '''        
        # if pulse detection trigger is on
        if self.startdetector == 1:
            # if button isn't being pressed
            if self.buttonpress == 0:
                # turn LED off when button isn't pressed
                t2ch1.pulse_width_percent(0)
                # if button not pressed for more than 1 second
                if utime.ticks_diff(utime.ticks_ms(),self.releaseTime)/1000 > 1:
                    # record pulse type (no press)
                    self.detectLED = 0
                    # reset release time for subsequent pulses
                    self.releaseTime = utime.ticks_ms()
                    # add pulse type to user input array
                    self.userpattern.append(self.detectLED)
                    # count each pulse inputted by user
                    self.UserPatternCounter()
            # if button is pressed
            elif self.buttonpress == 1:
                # turn LED on when button is pressed
                t2ch1.pulse_width_percent(100)
                # record time when button is pressed
                self.pressTime = utime.ticks_ms()
                # record time between this press and previous press
                self.userbetweentime = utime.ticks_diff(self.pressTime,self.releaseTime)/1000
                # if time between presses is between 0.9-1.5 seconds
                if 1-0.2*self.tolerance <= self.userbetweentime and self.userbetweentime <= 1+self.tolerance:
                    # record pulse type (no press)
                    self.detectLED = 0
                    # add pulse type to user input array
                    self.userpattern.append(self.detectLED)
                    # count each pulse inputted by user
                    self.UserPatternCounter()
                # set buttonpress indicator to 3 for hold
                self.buttonpress = 3
            # if button is held down
            elif self.buttonpress == 3:
                # if duration fo press exceeds 4 seconds
                if utime.ticks_diff(utime.ticks_ms(),self.pressTime)/1000 > 3+2*self.tolerance:
                    # turn LED off because round is now over
                    t2ch1.pulse_width_percent(0)
                    # record pulse type (too short/long press)
                    self.detectLED = 3
                    # add pulse type to user input array
                    self.userpattern.append(self.detectLED)
                    # count each pulse inputted by user
                    self.UserPatternCounter()
            # if button is released
            elif self.buttonpress >= 4:
                # turn LED off when button isn't pressed
                t2ch1.pulse_width_percent(0)
                # record time when button is released
                self.releaseTime = utime.ticks_ms()
                # record time that the button is presssed down
                self.userpresstime = utime.ticks_diff(self.releaseTime,self.pressTime)/1000
                # if time between button press and button release is between 0.5-1.5 seconds
                if 1-self.tolerance <= self.userpresstime and self.userpresstime <= 1+self.tolerance:
                    # record pulse type (short press)
                    self.detectLED = 1
                    # add pulse type to user input array
                    self.userpattern.append(self.detectLED)
                    # count each pulse inputted by user
                    self.UserPatternCounter()
                # if time between button press and button release is between 2-4 seconds
                elif 3-2*self.tolerance <= self.userpresstime and self.userpresstime <= 3+2*self.tolerance:
                    # record pulse type (long press)
                    self.detectLED = 2
                    # add pulse type to user input array
                    self.userpattern.append(self.detectLED)
                    # count each pulse inputted by user
                    self.UserPatternCounter()
                # if time between button press and button release is not between 0.5-1.5 or 2-4 seconds
                else:
                    # record pulse type (too short/long press)
                    self.detectLED = 3
                    # add pulse type to user input array
                    self.userpattern.append(self.detectLED)
                    # count each pulse inputted by user
                    self.UserPatternCounter()
                # turn buttonpress indicator off to prepare for next press
                self.buttonpress = 0

    def reset(self):
        '''
        @brief     This function resets variables after each round
        '''     
        self.patterncounter = 0 # set patterncounter back to 0
        self.userpatterncounter = 0 # reset user input counter
        self.userpattern = [] # reset user input
        self.buttonpress = 0 # reset button press trackers
        self.startdetector = 0 # reset pulse detection trigger
        self.pressTime = 0 # set press time back to 0
        self.releaseTime = 0 # set release time back to 0
        self.userpresstime = 0 # set user press time back to 0
        self.userbetweentime = 0 # set user between press time back to 0
    
    def FollowupAfterLoss(self):
        '''
        @brief     This function receives user followp after round loss
        '''        
        self.follow = True
        while self.follow:
            # receive user input
            self.followup = input("Would you like to try Round " + str(self.round) + " again? ").lower()
            # repeat current round while loop if "yes"
            if self.followup == 'yes':
                self.follow = False
                # repeat round (might be redundant)
                print("Round " + str(self.round) + "!")
                # reset if statement triggers
                self.roundstart = 1
                self.go = 1
                self.RoundTransitionTo(self.round)
            # exit program if "no"
            elif self.followup == 'no':
                self.follow = False
                print("Your win/loss record was: " + str(self.win) + "-" + str(self.lose))
                # exit program
                self.exit = 1
            # continue followup while loop until "yes" or "no" input received
            else:
                print('ERROR: please type "yes" to try again or "no" to leave.')
                self.follow = True
    
    def FollowupAfterRoundWin(self, nextround):
        '''
        @brief     This function receives user followp after round win
        @param nexstate The next round of the FSM 
        '''       
        self.follow = True
        while self.follow:
            # receive user input
            self.followup = input("Would you like to play Round " + str(nextround) + "? ").lower()
            # transition to next round while loop if "yes"
            if self.followup == 'yes':
                self.follow = False
                # transition to next round
                print("Round " + str(nextround) + "!")
                # reset if statement triggers
                self.roundstart = 1
                self.go = 1
                self.RoundTransitionTo(nextround)
            # exit program if "no"
            elif self.followup == 'no':
                self.follow = False
                print("Your win/loss record was: " + str(self.win) + "-" + str(self.lose))
                # exit program
                self.exit = 1
            # continue followup while loop until "yes" or "no" input received
            else:
                print('ERROR: please type "yes" to play round again or "no" to leave.')
                self.follow = True
    
    def FollowupAfterGameWin(self):
        '''
        @brief     This function receives user followp after game win 
        '''       
        self.follow = True
        while self.follow:
            # receive user input
            self.followup = input('Would you like to play again? ').lower()
            # transition to next round while loop if "yes"
            if self.followup == 'yes':
                self.follow = False
                print("Your win/loss record was: " + str(self.win) + "-" + str(self.lose))
                # reset win/loss record
                self.win = 0
                self.lose = 0
                # reset if statement triggers
                self.start = 1
                self.roundstart = 1
                self.go = 1
                # start game over
                self.RoundTransitionTo(self.R0_INIT)
            # exit program if "no"
            elif self.followup == 'no':
                self.follow = False
                print("Your win/loss record was: " + str(self.win) + "-" + str(self.lose))
                # exit program
                self.exit = 1
            # continue followup while loop until "yes" or "no" input received
            else:
                print('ERROR: please type "yes" to play again or "no" to leave.')
                self.follow = True
    
    def ButtonPress(self,IRQ_src):
        '''
        @brief     This callback function runs when the button is pressed.
        @param IRQ_src Specifies state of interrupt serivce request
        '''
        self.buttonpress = self.buttonpress + 1
            
    def PatternCounter(self):
        '''
        @brief     This function counts each blink.
        '''
        self.patterncounter = self.patterncounter + 1
    
    def PatternGenerator(self):
        '''
        @brief     This function generates a random LED pattern.
        '''
        self.randompattern = []
        # generate random patter of 20 integers (only 15 used in current set up)
        for i in range(0,20):
            # evaluate random integer between 0-2
            self.n = random.randint(0,2)
            # add each random integer to empty randompattern array
            self.randompattern.append(self.n)
    
    def UserPatternCounter(self):
        '''
        @brief     This function counts each unit inputted by user.
        '''
        self.userpatterncounter = self.userpatterncounter + 1

    def WIN(self):
        '''
        @brief     This function counts each round win.
        '''
        self.win = self.win + 1
        
    def LOSE(self):
        '''
        @brief     This function counts each round win.
        '''
        self.lose = self.lose + 1