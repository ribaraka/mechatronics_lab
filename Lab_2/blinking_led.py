"""@file        blinking_led.py
@brief          Cycles through several LED pulse patterns on Nucleo
@details        Implements a finite state machine, shown below, to cycle
                through three LED pulse patterns by clicking a user button on
                the Nucleo.
                
                Video:
                https://drive.google.com/file/d/1nbvR0DKdNV6b-GFImmbcYqiOmaySHbIr/view?usp=sharing
                
                Source Code:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_2/blinking_led.py
                
                Finite State Machine:
                @image html led_fsm.jpg
@author         Richard Barakat
@date           2/4/21
"""

import pyb
import utime
import math

def led(cmd):
    '''
    @brief     This function controls LED pulse pattern at a specific input.
    @param cmd A string specifying the desired operating condition of the 
               LED
    '''
    if cmd == 'square':
        # generate PWM with square wave duty (T = 1 sec)
        # if cycle time is less than 0.5 seconds, turn led to max brightness
        if utime.ticks_diff(utime.ticks_ms(),t_start)/1000 % 1 < 0.5:
            t2ch1.pulse_width_percent(100) 
        # rest of cycle time turn LED off
        else:
            t2ch1.pulse_width_percent(0) 
    elif cmd == 'sine':
        # generate PWM with sine wave duty (T = 10 sec)
        t2ch1.pulse_width_percent(100*0.5*(math.sin(((1/10)*2*math.pi)*(utime.ticks_diff(utime.ticks_ms(),t_start)/1000))+1))
    elif cmd == 'saw':
        # generate PWM with sawtooth duty (T = 1 sec)
        t2ch1.pulse_width_percent(100*(utime.ticks_diff(utime.ticks_ms(),t_start)/1000 % 1))

def ButtonCallback(IRQ_src):
    '''
    @brief     This callback function runs when the button is pressed.
    @param IRQ_src Specifies state of interrupt serivce request
    '''
    global button
    button += 1

# Initialize variables

## @brief Defines state of FSM
#  @details This variable describes the current state of the FSM and changes 
#           to the subsequent state after each button press
state = 0

## @brief Defines start time for LED pulse patterns
#  @details This variable stores the time when the button is pressed and is 
#           used to determine the duty cycle for the state after each press
t_start = 0 # start time not set

## @brief Defines whether the button has been pressed or not
#  @details This variable increases by +1 as defined in def ButtonCallback 
#           and is used to temporarily denote whether the button has been 
#           during a given state
button = 0 # button initially 0

## @brief Defines output pin associated with LED
#  @details This variable defines pin A5 on the Nucleo which is used for 
#           performing the PWM
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## @brief Defines timer to be used for LED
#  @details This variable defines Timer 2 to be used at 10000Hz for the LED
tim2 = pyb.Timer(2, freq = 10000)
## @brief Defines channel to be used for LED
#  @details This variable defines channel 1 to be used to perform PWM with pin
#           A5
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## @brief Defines input pin associated blue button, B1
#  @details This variable defines pin C13 on the Nucleo which corresponds to 
#           the blue user button
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
## @brief Defines external interrupt to associate callback function with pin
#  @details This variable defines the external interrupt that will trigger def
#           ButtonCallback to run when button, B1, is pressed
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=ButtonCallback)

print('Welcome to the blinking LED program! Please press the blue button on '
      'the Nucleo to cycle through three different LED pulse patterns.') 

while True:
    try:
        if state == 0:
            # run state 0 (INIT) code
            # if button is pressed, transition to state 1
            if button >= 1: # if button press
                print('Displaying Square Wave Pattern')  
                button = 0 # reset button
                t_start = utime.ticks_ms() # record start time at button press
                state = 1 # update state for next iteration
        
        elif state == 1:
            # run state 1 (square wave) code
            # command LED to generate PWM with sine duty
            led('square')
            # if button is pressed, transition to state 2
            if button >= 1: # if button pressed
                print('Displaying Sine Wave Pattern')  
                button = 0 # reset button
                t_start = utime.ticks_ms() # record start time at button press
                state = 2 # update state for next iteration
                            
        elif state == 2:
            # run state 2 (sine wave) code
            # command LED to generate PWM with sine duty
            led('sine')
            # if button is pressed, transition to state 3
            if button >= 1: # if button pressed
                print('Displaying Sawtooth Wave Pattern')
                button = 0 # reset button
                t_start = utime.ticks_ms() # record start time at button press
                state = 3 # update state for next iteration
        
        elif state == 3:
            # run state 3 (sawtooth wave) code
            # command LED to generate PWM with sawtooth duty
            led('saw')
            # if button is pressed, transition to state 1
            if button >= 1: # if button pressed
                print('Displaying Square Wave Pattern')
                button = 0 # reset button
                t_start = utime.ticks_ms() # record start time at button press
                state = 1 # update state for next iteration
                       
    except KeyboardInterrupt:
        # catches "Ctrl-C" from keyboard to turn off LED, disable the
        # interrupt, and break loop
        t2ch1.pulse_width_percent(0)    
        ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=None)
        break
