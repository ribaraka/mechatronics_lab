"""@file        main_ENCdriver_Week_2.py
@brief          Records encoder position and returns value to user.
@details        Receives user input for desired enocder measurement and sends 
                user request to Nucleo via serial. Nucleo reads via uart and 
                evaluate the desired encoder metric. If the user asks for 
                data collection, the Nucelo will record encoder postion values
                for 30 seconds or until the user interrupts prematurely and 
                sends the data via uart to the PC.

                Class Reference Page:
                https://ribaraka.bitbucket.io/classENCdriver__Week__2_1_1ENCdriver__Week__2.html    
                    
                UI Reference Page:
                https://ribaraka.bitbucket.io/UI__front__ENC_8py.html
                
                UI Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_2/UI_front_ENC.py
                
                Class Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_2/ENCdriver_Week_2.py
                
                Main Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_2/main_ENCdriver_Week_2.py

                Finite State Machine:
                @image html encoder_fsm.jpg
                
                Example Encoder Plot:
                @image html encoder_plot.jpg    
@author         Richard Barakat
@date           3/18/21
"""

# import encoder driver class file
from ENCdriver_Week_2 import ENCdriver_Week_2

# encoder 1 class
task = ENCdriver_Week_2(1)

while True:
    try:
        # execute code
        task.EncoderFSM()
        
    except KeyboardInterrupt:
        # This except block catches "Ctrl-C" from the keyboard to end the
        # while(True) loop when desired
        print('Ctrl-c has been pressed')
        break
