"""@file        UI_front_ENC.py
@brief          Sends user inputs to and receives encoder data from the Nucleo.
@details        Implements serial communication to send data to the Nucelo.
                The user will input a character to start data collection, send 
                that information to the Nucleo to read the encoder position, 
                receive another user input when the user is ready to stop 
                collecting data, send that information to the Nucleo, and 
                receive and plot the encoder data from the Nucleo.

                Class Reference Page:
                https://ribaraka.bitbucket.io/classENCdriver__Week__2_1_1ENCdriver__Week__2.html      
                    
                Main Reference Page:
                https://ribaraka.bitbucket.io/main__ENCdriver__Week__2_8py.html
                
                UI Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_2/UI_front_ENC.py
                
                Class Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_2/ENCdriver_Week_2.py
                
                Main Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_2/main_ENCdriver_Week_2.py
                
@author         Richard Barakat
@date           3/18/21
"""

import serial
import csv
from matplotlib import pyplot
import time

# initialize variables
collect = 0
t_start = 0
t_end = 0
zero_pos = 0
position = 0
delta = 0
times = []
values = []

def getData():
    '''
    @brief     This function receives data from Nucleo when a 's' is input
    '''
    timeout = 0
    while ser.in_waiting == 0:
        timeout+=1
        if timeout > 1_000_000:
            return None
    # read line of data from Nucleo via serial communication
    return ser.readline().decode()

# The with block here will automatically close the serial port once the block 
# ends you could just make a serial object as usual instead
with serial.Serial(port='COM4',baudrate=115273,timeout=1) as ser:
    while True:
        if collect == 0:
            # ask user if they wnat to zero encoder, read position, read delta, or collect data for 30 seconds
            user_in = input("Enter 'z' to zero the encoder\r\n"
                            "Enter 'p' to print the encoder position\r\n"
                            "Enter 'd' to print the encoder delta\r\n"
                            "Enter 'g' to start collecting encoder data\r\n"
                            "Enter 'q' to quit\r\n>>")
            if user_in == 'z' or user_in == 'Z':
                # send user input to Nucleo via serial communication
                ser.write('z'.encode())
                # receive zero position from Nucleo via uart
                zero_pos = getData()
                print(zero_pos)
            elif user_in == 'p' or user_in == 'P':
                # send user input to Nucleo via serial communication
                ser.write('p'.encode())
                # receive position from Nucleo via uart
                position = getData()
                print(position)
            elif user_in == 'd' or user_in == 'D':
                # send user input to Nucleo via serial communication
                ser.write('d'.encode())
                # receive zero position form Nucleo via uart
                delta = getData()
                print(delta)
            elif user_in == 'g' or user_in == 'G':
                # send user input to Nucleo via serial communication
                ser.write('g'.encode())
                # record time when data collection begins
                t_start = time.time()
                # transition to asking user when to stop collecting data
                collect = 1
            else:
                pass
                
        if collect == 1:
            # ask user to stop collecting data
            user_in = input("Enter 's' to get data from the Nucleo\r\n>>")
            if user_in == 's' or user_in == 'S':
                # send user input to Nucleo via serial communication
                ser.write('s'.encode())
                # record time when data collection ends
                t_end = time.time()
                # transition to collection if statement
                collect = 2
            else:
                pass
        
        if collect == 2:
            # if serial com port is not waiting, receive data from Nucleo
            if ser.in_waiting != 0:
                # retrieve data from Nucleo via serial communication
                n = getData()
                # Remove line endings
                strippedString = n.strip()
                # split on the commas
                splitStrings = strippedString.split(',')
                # store times
                times.append(float(splitStrings[0]))
                # store values
                values.append(float(splitStrings[1]))
                if abs(max(times)-(t_end-t_start)) < 0.1 or max(times) > 29.8:
                    break
                    
        if user_in == 'q' or user_in == 'Q':
            print('Thanks')
            break

# plot results
pyplot.figure()
# plot time as independent variable and values as dependent
pyplot.plot(times, values)
# format plot
pyplot.xlabel('Time [sec]')
pyplot.ylabel('Encoder Position [ticks]')

# save as CSV file
with open('Encoder_1_data.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for m in range(0,len(times)):
        writer.writerow([times[m], values[m]])
        m += 1