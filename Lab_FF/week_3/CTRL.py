from pyb import UART
import utime

myuart = UART(2)

# import motor driver class file
from MOTdriver import MOTdriver

# import encoder driver class file
from ENCdriver import ENCdriver

# import encoder driver class file
from ClosedLoop import ClosedLoop

class CTRL:
    ''' @brief          Imports all driver code
        @details        The class will evaluate the postion of the encoder 
                        (1 or 2) specified by the user in the main file. Data 
                        can be returned at specific instances or for a max
                        duration of 30 seconds.
    '''
    
    def __init__(self):
        ''' @brief         Constructs a CTRL.CTRL object. Returns and send 
            @details       data to PC.
                           
                           Class Source:
                           https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/CTRL.py
        '''
        # motor 1 class
        self.task_mot = MOTdriver(1)

        # encoder 1 class
        self.task_enc = ENCdriver(1)
        
        ## current stae of the CTRL fsm
        self.state = 0
        
        ## artifical time value that enables encoder to be sampled at each timestep
        self.t = 0
        
        ## array in which time values are stored
        self.times = []
        
        ## array in which encoder positions are stored
        self.speeds = []
        
        ## index for n for collection for loop
        self.n = 0
        
        ## index for n for sending for loop
        self.m = 0
        
        ## current motor position
        self.mot_pos = 0
        
        ## current motor delta
        self.mot_delta = 0
        
        ## current motor velocity
        self.mot_vel = 0
        
        ## current start time for data collection
        self.t_start = 0
        
        ## current state of the CTRL FSM
        self.state = 0
        
        ## current timestep of the fsm
        self.timestep = self.task_enc.timestep
        
        ## current user input
        self.user = 0

    def CTRL_FSM(self):
        '''
        @brief     The FSM for the driver control
        '''    
        # initial state
        if self.state == 0:
            # calculate next time
            self.t = self.t + self.timestep
            # record time when data collection starts
            self.t_start = utime.ticks_ms()
            # transition to state 1
            self.state = 1
        
        # individual data point collection and zeroing state    
        if self.state == 1:
            # after every timestep...
            if utime.ticks_diff(utime.ticks_ms(),self.t_start)/1000 >= self.t:
                # record encoder positon after each timestep
                self.runENC()
                # calculate next time
                self.t = self.t + self.timestep
                # if character received via uart
                if myuart.any():
                    # Read user input and turn it into a string
                    self.user = myuart.read(1).decode()
                    if self.user == "z":   
                        # zero the encoder position
                        self.task_enc.position = 0
                        self.mot_pos = 0
                        myuart.write('Encoder position set to ' + str(self.mot_pos))
                        
                    elif self.user == "p":
                        # print the encoder position
                        myuart.write('Encoder position is ' + str(self.mot_pos))
                        
                    elif self.user == "d":
                        # print the encoder delta
                        print(self.mot_delta)
                        myuart.write('Encoder delta is ' + str(self.mot_delta))
                        
                    elif self.user == "g":
                        # set time to 0
                        self.t = 0                    
                        # record inital conditions
                        self.times.append(self.t)
                        # evaluate inital motor velocity
                        self.speeds.append(self.mot_vel)
                        # count data point
                        self.n += 1
                        # calculate next time
                        self.t = self.t + self.timestep
                        # record time when data collection starts
                        self.t_start = utime.ticks_ms()
                        # enable motor
                        self.task_mot.enable()
                        # transition to state 2 to begin collecting data
                        self.state = 2
                        
                    else:
                        pass
                else:
                    pass
                
        # data collection over 30 seconds / premature stop state
        if self.state == 2:
            # if the current time is less than the duration
            if self.t <= self.task_enc.duration:
                # after every timestep..
                if utime.ticks_diff(utime.ticks_ms(),self.t_start)/1000 >= self.t:
                    # record encoder positon after each timestep
                    self.runENC()
                    # get refernce velocity
                    self.getRef()
                    # get proportional constant
                    self.getKp()
                    # closed loop task
                    self.task_cl = ClosedLoop(self.k_p, self.ref, self.mot_delta, self.timestep)
                    # apply cloded loop control to motor and evaluate velcocity and PWM signal
                    self.runCL()
                    # run the motor at the PWM signal outputte dby the controller
                    self.runMOT()
                    # store timestamps
                    self.times.append(self.t)
                    # store motor velocity
                    self.speeds.append(self.mot_vel)
                    # trigger counter to prepare for next index to be evaluated
                    self.n += 1
                    # calculate next time
                    self.t = self.t + self.timestep
                    if myuart.any():
                       # Read user input and turn it into a string
                       self.user = myuart.read(1).decode()
                       if self.user == "s":
                           # end data collection and send FSM to state 3
                           self.state = 3
                       else:
                           pass
                    else:
                        pass  
        
        # data send via uart state
        if self.state == 3:
            # send data to PC
            for self.m in range(0,self.n):
                # generate string and send (write) to PC frontend
                myuart.write('{:}, {:}\r\n'.format(self.times[self.m], self.speeds[self.m]))
                self.m += 1
            # enable motor
            self.task_mot.disable()
            self.state = 0
    
    def getRef(self):
        '''
        @brief     defines desired velocity in rad/s
        '''
        self.ref = 100
    
    def getKp(self):
        '''
        @brief     defines proportional control constant, Kp
        '''
        self.k_p = 0.5

    def runENC(self):
        '''
        @brief     reads encoder position
        '''
        # execute code
        self.task_enc.Delta()
        # encoder position
        self.mot_pos = self.task_enc.position
        # enoder delta
        self.mot_delta = self.task_enc.delta
        
    def runCL(self):
        '''
        @brief     evaluates omega and runs closed loop controller
        '''
        # execute controller code
        self.task_cl.run()
        # motor velocity
        self.mot_vel = self.task_cl.vel
        # motor PWM signal
        self.mot_L = self.task_cl.L

    def runMOT(self):
        '''
        @brief     runs motor with PWM signal computted by controller
        '''
        # set motor duty
        self.duty_mot = self.mot_L
        # execute code
        self.task_mot.setDuty(self.duty_mot)