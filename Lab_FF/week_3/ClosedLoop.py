from pyb import UART
from math import pi

myuart = UART(2)

class ClosedLoop:
    ''' @brief      Impelements a closed loop P controller for motors.  
        @details    This class will run a closedloop control for a specified 
                    reference value and input measurements.
    '''
    
    def __init__(self, k_p, ref, encoder_delta, encoder_timestep):
        ''' @brief         Constructs a ClosedLoop.ClosedLoop object. Returns
            @details       position of motor 1 or 2 as requested by user.
                           
                           Class Source:
                           https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/ClosedLoop.py
            @param k_p     The proportional constant
            @param ref     Desired velocity of theoutput shaft
            @param encoder_delta     delta from encoder
            @param encoder_timestep  timestep from encoder
        '''
        ## define proportional control constant
        self.k_p = k_p
        
        ## define which motor is being used
        self.ref = ref
        
        ## enocder measurements from encoder driver
        self.encoder_delta = encoder_delta
        
        ## encodefr timestep
        self.timestep = encoder_timestep
                
        ## max pwm signal is 100
        self.max = 100
    
    def getVel(self):
        '''
        @brief     computes shaft velocity in rad/s
        '''
        ## convert encoder delta measurements to radians
        self.delta_rad = self.encoder_delta*((2*pi)/5255)
        ## measured angular velocity in rad/s
        self.vel = self.delta_rad/self.timestep
    
    def run(self):
        '''
        @brief     controlled duty for the motor PWM
        '''     
        self.getVel()
        self.L = min(self.k_p*(self.ref-self.vel), self.max)
     