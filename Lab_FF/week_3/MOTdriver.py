import pyb
from pyb import UART

myuart = UART(2)

class MOTdriver:
    ''' @brief      Impelements a motor driver for the ME 405 board    
        @details    This class will run the motor specified the user (1 or 2) 
                    at the duty required by the controller.
    '''
    
    def __init__(self, mot_num):
        ''' @brief         Constructs a MOTdriver.MOTdriver object. Returns
            @details       position of motor 1 or 2 as requested by user.
                           
                           Class Source:
                           https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/MOTdriver.py
            @param mot_num The motor selected by the user
        '''
        
        ## define which motor is being used
        self.mot_num = mot_num
        
        if self.mot_num == 1:
            ## Pins and timers used for motor 1 driver
            self.pinIN1 = pyb.Pin(pyb.Pin.cpu.B4)
            self.pinIN2 = pyb.Pin(pyb.Pin.cpu.B5)
            self.tim3 = pyb.Timer(3, freq = 10000)
            self.t3ch1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.pinIN1)
            self.t3ch2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.pinIN2)
            # current motor speed
            self.t3ch1.pulse_width_percent(0) 
            self.t3ch2.pulse_width_percent(0) 
            
        if self.mot_num == 2:
            ## Pins and timers used for motor 2 driver
            self.pinIN3 = pyb.Pin(pyb.Pin.cpu.B0)
            self.pinIN4 = pyb.Pin(pyb.Pin.cpu.B1)
            self.tim3 = pyb.Timer(3, freq = 10000)
            self.t3ch3 = self.tim3.channel(3, pyb.Timer.PWM, pin=self.pinIN3)
            self.t3ch4 = self.tim3.channel(4, pyb.Timer.PWM, pin=self.pinIN4)
            # current motor speed
            self.t3ch3.pulse_width_percent(0) 
            self.t3ch4.pulse_width_percent(0) 
        
        ## define nSLEEP pin
        self.pin_nSLEEP = pyb.Pin('A15', pyb.Pin.OUT_PP)
        
    def enable(self):
        '''
        @brief     Enables motor by setting nSLEEP pin to high
        '''    
        # set nSLEEP pin high
        self.pin_nSLEEP.value(True)
        
    def disable(self):
        '''
        @brief     Disables motor by setting nSLEEP pin to low
        '''    
        # set nSLEEP pin low
        self.pin_nSLEEP.value(False)
        
    def setDuty(self, duty):
        '''
        @brief      Sets duty cycle for motor
        @details    This method sets the duty cycle to be sent to the motor to 
                    the given level. Positive valeus cause effor in one 
                    direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal 
                    sent to the motor
        '''   
        self.duty = duty
        # motor 1
        if self.mot_num == 1:
            # if duty > 0 => forward
            if self.duty > 0:
                # IN2 low
                self.t3ch2.pulse_width_percent(0)
                # IN1 high
                self.t3ch1.pulse_width_percent(self.duty)  
            # if duty = 0
            elif self.duty == 0:
                # IN1 low
                self.t3ch1.pulse_width_percent(0) 
                # IN2 low
                self.t3ch2.pulse_width_percent(0) 
            # if duty < 0 => reverse
            elif self.duty < 0:
                # IN1 low
                self.t3ch1.pulse_width_percent(0)
                # IN2 high
                self.t3ch2.pulse_width_percent(abs(self.duty)) 
                
        # motor 2
        if self.mot_num == 2:
            # if duty > 0 => forward
            if self.duty > 0:
                # IN4 low
                self.t3ch4.pulse_width_percent(0)
                # IN3 high
                self.t3ch3.pulse_width_percent(self.duty)  
            # if duty = 0
            elif self.duty == 0:
                # IN3 low
                self.t3ch3.pulse_width_percent(0) 
                # IN4 low
                self.t3ch4.pulse_width_percent(0) 
            # if duty < 0 => reverse
            elif self.duty < 0:
                # IN3 low
                self.t3ch3.pulse_width_percent(0)
                # IN4 high
                self.t3ch4.pulse_width_percent(abs(self.duty))         