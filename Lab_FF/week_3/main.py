"""@file        main.py
@brief          Records shaft velocity and sends data to user
@details        Receives user input for desired shaft velocity and sends 
                user request to Nucleo via serial. Nucleo reads via uart and 
                evaluate the desired encoder metric or start the motor and 
                collect velocity data until the user interrupts. Then, the 
                Nucleo will send the data via uart to the PC.

                Class Reference Pages:
                
                CTRLtask - https://ribaraka.bitbucket.io/classCTRL_1_1CTRL.html
                
                Encoder Driver - https://ribaraka.bitbucket.io/classENCdriver_1_1ENCdriver.html
                
                Motor Driver - https://ribaraka.bitbucket.io/classMOTdriver_1_1MOTdriver.html
                
                Closed Loop Driver - https://ribaraka.bitbucket.io/classClosedLoop_1_1ClosedLoop.html

                UI Reference Page: https://ribaraka.bitbucket.io/UI__frontend_8py.html
                
                UI Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/UI_frontend.py
                
                CTRL Class Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/CTRL.py
                
                Encoder Class Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/ENCdriver.py
                
                Motor Class Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/MOTdriver.py
                
                Closed Loop Class Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/ClosedLoop.py
                
                Main Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/main.py

                Finite State Machine:
                @image html CTRL_fsm.jpg
                
                Task Diagram:
                @image html task_fsm.jpg
                
                Velocity Plot, Kp = 0.5:
                @image html kp_05.jpg
                
                Velocity Plot, Kp = 1:
                @image html kp_1.jpg
                
                Velocity Plot, Kp = 5:
                @image html kp_5.jpg
                
@author         Richard Barakat
@date           3/18/21
"""

# import CTRL task
from CTRL import CTRL

# CTRL class
task = CTRL()

while True:
    try:
        # execute code
        task.CTRL_FSM()
        
    except KeyboardInterrupt:
        # This except block catches "Ctrl-C" from the keyboard to end the
        # while(True) loop when desired
        print('Ctrl-c has been pressed')
        break
