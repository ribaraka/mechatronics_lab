import pyb
from pyb import UART
import utime

myuart = UART(2)

class ENCdriver:
    ''' @brief          Returns position data for specified encoder.
        @details        The class will evaluate the postion of the encoder 
                        (1 or 2) specified by the user in the main file. Data 
                        can be returned at specific instances or for a max
                        duration of 30 seconds.
    '''
    
    def __init__(self, enc_num):
        ''' @brief         Constructs a ENCdriver.ENCdriver object. Returns
            @details       position of encoder 1 or 2 as requested by user.
                           
                           Class Source:
                           https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_3/ENCdriver.py
            @param enc_num The encoder selected by the user
        '''
        
        ## define which encoder is being used
        self.enc_num = enc_num
        
        # encoder 1 pins
        if self.enc_num == 1:
            ## Pins and timers used for encoder 1 driver
            self.pinPB6 = pyb.Pin(pyb.Pin.cpu.B6)
            self.pinPB7 = pyb.Pin(pyb.Pin.cpu.B7)
            self.tim4 = pyb.Timer(4, prescaler = 0, period = 65535)
            self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_AB, pin=self.pinPB6)
            self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_AB, pin=self.pinPB7)
        
            # encoder 2 pins
        if self.enc_num == 2:
            ## Pins and timers used for encoder 2 driver
            self.pinPC6 = pyb.Pin(pyb.Pin.cpu.C6)
            self.pinPC7 = pyb.Pin(pyb.Pin.cpu.C7)
            self.tim3 = pyb.Timer(3, prescaler = 0, period = 65535)
            self.t3ch1 = self.tim3.channel(1, pyb.Timer.ENC_AB, pin=self.pinPC6)
            self.t3ch2 = self.tim3.channel(2, pyb.Timer.ENC_AB, pin=self.pinPC7)
            
        ## current state of the encoder finite state machine
        self.state = 0
        
        ## current value of the encoder
        self.encoder = 0
        
        ## current position of the shaft
        self.position = 0
        
        ## current delta of the encoder (between current nad previous values)
        self.delta = 0
        
        ## timestep at which encoder values are recorded
        self.timestep = 0.15 # seconds
        
        ## current time
        self.t_start = 0
        
        ## current user input that controls how task behaves
        self.user = 0
        
        ## duration of data collection
        self.duration = 30
        
        ## counter for data collection for loop
        self.send = 0
        
        self.enc_1 = 0
        
        self.enc_2 = 0
        
    def Update(self):
        '''
        @brief     This updates the recorded position of the Encoder.
        '''
        if self.enc_num == 1:
            self.encoder = self.tim4.counter()
        if self.enc_num == 2:
            self.encoder = self.tim3.counter()
    
    def Delta(self):
        '''
        @brief     Record ticks between the current and previous enc values.
        '''
        # record previous encoder values
        self.enc_1 = self.enc_2
        # update encoder value
        self.Update()
        # record current encoder value
        self.enc_2 = self.encoder
        # evaluate delta between current and previous encoder values
        self.delta = self.enc_2 - self.enc_1
        # adjust encoder valeus to account for overflow
        if -65535/2 < self.delta < 65535/2:
            # good delta reading so pass
            pass
        elif self.delta > 65535/2:
            # bad delta reading (underflow) so subtract encoder timer period
            self.delta = self.delta - 65535
        elif self.delta < -65535/2:
            # bad delta reading (overflow) so add encoder timer period
            self.delta = self.delta + 65535
        # determine encoder position
        self.getPosition()
            
    def getPosition(self):
        '''
        @brief     This returns most recently updated position of encoder.
        '''
        self.position = self.position + self.delta
    
    def setPosition(self,position):
        '''
        @brief     This zeros the encoder position.
        '''
        self.position = position
        
    def reset(self):
        '''
        @brief     This function resets variables after sending data
        '''     
        self.state = 0
        self.encoder = 0
        self.position = 0
        self.delta = 0
        self.times = []
        self.position_values = []
        self.t_start = utime.ticks_ms()
        self.user = 0