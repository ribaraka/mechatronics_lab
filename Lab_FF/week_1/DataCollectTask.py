from pyb import UART
from math import exp, sin, pi
import utime

myuart = UART(2)

class DataCollectTask:
    ''' @brief          DataCollectTask solves the decaying sine function.
        @details        An object of class DataColectTask.DataCollectTask 
                        can solve a decaying since function on the Nucleo 
                        over a specifed duration and send the data to the 
                        PC script. Multiple DataCollectTask can be used at 
                        once to send data from multiple sources to the PC 
                        script.
    '''
    
    def __init__(self, duration, timestep):
        ''' @brief         Constructs a DataCollectTask.DataCollectTask object.
            @details       The DataCollectTask.DataCollectTask object can be 
                           run over a specifed duration with a specified 
                           timestep.
                           
                           Class Source:
                           https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_1/DataCollectTask.py
                           
            @param duration The total time of data collection
            @param timestep The time between data points
        '''
        ## The name of the food item
        self.duration = duration
        
        ## The name of the food item
        self.timestep = timestep
        
        ## The user input
        self.user = 0
        
        ## The current state of the FSM
        self.state = 0
        
        ## The time value of collected data
        self.t = 0
        
        ## The time at which data collection begins
        self.t_start = 0
        
        ## The array where time values will be stored
        self.times = []
        
        ## The array where the calculated output will be stored
        self.values = []
        
        ## Counter for data storage loop
        self.n = 0
        
        ## Counter for data dend loop
        self.m = 0
        
    def DataCollectFSM(self):
        '''
        @brief     This is the FSM for evaluating, storing, and sending data.
        '''
        if self.state == 0:
            # reset variables
            self.reset()
            if myuart.any():
                # Read user input and turn it into a string
                self.user = myuart.read(1).decode()
                if self.user == "g":
                    # record inital conditions
                    self.times.append(self.t)
                    # evaluate function in terms of time; store from n = 0 to n = 3001
                    self.values.append(exp(-self.t/10)*sin(2*pi/3*self.t))
                    # count each data point
                    self.n += 1
                    # calculate next time
                    self.t = self.t + self.timestep
                    # measure time when data collection begins
                    self.t_start = utime.ticks_ms()
                    # send FSM to state 1
                    self.state = 1
                else:
                    pass
    
        # collect data
        if self.state == 1:
             # if the current time is less than the duration
             if self.t <= self.duration:
                 # record data after each timestep elapses (behaves as timer for data collection)
                 if utime.ticks_diff(utime.ticks_ms(),self.t_start)/1000 >= self.t:
                    # store timestamps
                    self.times.append(self.t)
                    # evaluate function in terms of time; store from n = 0 to n = 3001
                    self.values.append(exp(-self.t/10)*sin(2*pi/3*self.t))
                    # trigger counter to prepare for next index to be evaluated
                    self.n += 1
                    # calculate next time
                    self.t = self.t + self.timestep
                    if myuart.any():
                       # Read user input and turn it into a string
                       self.user = myuart.read(1).decode()
                       if self.user == "s":
                           # send FSM to state 2
                           self.state = 2
                       else:
                           pass
                    else:
                        pass
                   
             else:
                 # send FSM to state 2
                 self.state = 2
                
        # send data to frontend               
        if self.state == 2:
            for self.m in range(0,self.n):
                # generate string and send (write) to PC frontend
                myuart.write('{:}, {:}\r\n'.format(self.times[self.m], self.values[self.m]))
                self.m += 1
            self.state = 0        
        
    def reset(self):
        '''
        @brief     This function resets variables after sending data
        '''     
        self.user = 0
        self.state = 0
        self.t = 0
        self.n = 0
        self.m = 0
        self.times = []
        self.values = []