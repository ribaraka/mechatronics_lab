"""@file        main_DataCollect_Week_1.py
@brief          Generates, stores, and sends data of decaying sine function.
@details        Implements a finite state machines, shown below, to evalaute a
                decaying sine function over a specified duration and store the 
                results. The results are sent to the PC script at the request 
                of the user or when the time duration is complete.

                Class Reference Page:
                https://ribaraka.bitbucket.io/classDataCollectTask_1_1DataCollectTask.html    
                    
                UI Reference Page:
                https://ribaraka.bitbucket.io/UI__front__DataCollect_8py.html
                
                UI Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_1/UI_front_DataCollect.py
                
                Class Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_1/DataCollectTask.py
                
                Main Source:
                https://bitbucket.org/ribaraka/mechatronics_lab/src/master/Lab_FF/week_1/main_DataCollect_Week_1.py    
                    
                Finite State Machine:
                @image html datacollection_fsm.jpg
                
                Exapmle Data Collection Plot Stopped Prematurely:
                @image html data_plot.jpg 
@author         Richard Barakat
@date           3/18/21
"""

# import data collection class file
from DataCollectTask import DataCollectTask

# Run task for druation of 30 sec with timestep of 0.1 sec
task = DataCollectTask(30,0.15)

while True:
    try:
        # execute code
        task.DataCollectFSM()
        
    except KeyboardInterrupt:
        # This except block catches "Ctrl-C" from the keyboard to end the
        # while(True) loop when desired
        print('Ctrl-c has been pressed')
        break
