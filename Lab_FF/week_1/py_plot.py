from matplotlib import pyplot
from array import array
from math import exp, sin, pi

# create array for time values for 30 seconds
duration = 30 # seconds
timestep = 0.01 # seconds
length = int(duration/timestep+1) # use int() bc range only reads integers
times  = array('f', list(time*timestep for time in range(0,length)))

# create empty array to store output data values
values = array('f',length*[0])

# initialize for loop
n = 0

# use for loop to evaluate function at each timestep
for time in times:
    # evaluate function in terms of time; store from n = 0 to n = 3001
    values[n] = exp(-time/10)*sin(2*pi/3*time)
    # trigger counter to prepare for next index to be evaluated
    n += 1
    
# plot data vs time using matplotlib

pyplot.figure()
# plot time as independent variable and values as dependent
pyplot.plot(times, values)
pyplot.xlabel('Time')
pyplot.ylabel('Data')

# EXAMPLE
# print data as csv to console
    # len() measures number of items in times (should equal to length)
for n in range(len(times)):
    print('{:}, {:}'.format(times[n], values[n]))
    # print(str(times[n]) + ', ' + str(values[n]))

# EXAMPLE
# build a single line string, strip special characters, split on the 
# commmas, then float the entries
num1 = 1
num2 = 2
# Generate string (need to read this with ser.read() in PC script)
myLineString = '{:}, {:}\r\n'.format(num1, num2)
print(myLineString)

# remove line endings
myStrippedString = myLineString.strip()
print(myStrippedString)

# split on commas
mySplitStrings = myStrippedString.split(',')
print(mySplitStrings)

# Convert entries to numbers from strings
myNum1 = float(mySplitStrings[0])
print(myNum1)
myNum2 = float(mySplitStrings[1])
print(myNum2)